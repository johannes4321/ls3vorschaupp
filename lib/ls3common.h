#pragma once

#include <cstdint>
#include <string>

namespace ls3 {
enum struct FileFormat : int32_t { BMP, JPG, TGA, PNG, DDS, PPM, DIB, HDR };

enum struct AnsichtModus : int8_t {
    Fahrzeugansicht,  // 0: Fahrzeugansicht
    Seite1,           //   1: an Objektabmesssungen angepasster Standort eine Seite,
    Seite2            // 2: andere Seite
};

struct Size {
    int32_t x, y;
};

struct request {
    char* filename;
    char* target;
    AnsichtModus mode;
    Size size;
    FileFormat format;

    bool valid() const noexcept {
        return filename && target && mode >= AnsichtModus::Fahrzeugansicht &&
               mode <= AnsichtModus::Seite2 && size.x > 0 && size.x < 1000 && size.y > 0 &&
               size.y < 1000 && format >= FileFormat::BMP && format <= FileFormat::HDR;
    }
};

template <typename impl>
class ls3interface {
   public:
    bool operator()(const char* DateiName, const char* BMPDateiname, AnsichtModus Modus, Size size,
                    FileFormat format) const noexcept {
        auto& derived = static_cast<const impl&>(*this);
        return derived.generate(DateiName, BMPDateiname, Modus, size, format);
    }

    bool operator()(const request& req) const noexcept {
        return req.valid() && (*this)(req.filename, req.target, req.mode, req.size, req.format);
    }
};

inline std::string fullFilename(const char* base, FileFormat format) {
    std::string basename{base};
    switch (format) {
        case FileFormat::BMP:
            return basename + ".bmp";
        case FileFormat::JPG:
            return basename + ".jpg";
        case FileFormat::TGA:
            return basename + ".tga";
        case FileFormat::PNG:
            return basename + ".png";
        case FileFormat::DDS:
            return basename + ".dds";
        case FileFormat::PPM:
            return basename + ".ppm";
        case FileFormat::DIB:
            return basename + ".dib";
        case FileFormat::HDR:
            return basename + ".hdr";
    }

    return "This should never happen ls3::fullFilename switch didn't hit a case";
}

inline std::string fullFilename(const request& r) {
    return fullFilename(r.target, r.format);
}
}  // namespace ls3
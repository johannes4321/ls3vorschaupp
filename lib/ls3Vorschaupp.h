﻿#pragma once

#include "ls3common.h"

#include <cstdint>
#include <string>

#include <Windows.h>

namespace ls3 {

class ls3Vorschaupp : public ls3interface<ls3Vorschaupp> {
    friend ls3interface<ls3Vorschaupp>;
    typedef int8_t(_stdcall* ls3Vorschau_t)(const char* Arbeitsverzeichnis, const char* DateiName,
                                            const char* BMPDateiname, AnsichtModus Modus,
                                            HWND Handle, int32_t BMPWidth, int32_t BMPHeight,
                                            FileFormat BMPFormat);

    std::string baseDir;

    HWND hwnd;
    HINSTANCE hinstLib{};
    ls3Vorschau_t _ls3Vorschau{};

    void doLoad();

   public:
    ls3Vorschaupp(HWND hwnd, LPCSTR dllDir, LPCSTR baseDir);
    ls3Vorschaupp(HWND hwnd, LPCWSTR dllDir, LPCSTR baseDir);

    ls3Vorschaupp(LPCSTR dllDir, LPCSTR baseDir) : ls3Vorschaupp{0, dllDir, baseDir} {
    }
    ls3Vorschaupp(LPCWSTR dllDir, LPCSTR baseDir) : ls3Vorschaupp{0, dllDir, baseDir} {
    }

    // Until the destructor is fixed we could allow copies, but once that is done we'd break the
    // interface ...
    ls3Vorschaupp(const ls3Vorschaupp&) = delete;
    ls3Vorschaupp& operator=(const ls3Vorschaupp&) = delete;

    ls3Vorschaupp(ls3Vorschaupp&&) = default;

    ~ls3Vorschaupp();

   private:
    bool generate(const char* DateiName, const char* BMPDateiname, AnsichtModus Modus, Size size,
                  FileFormat format) const noexcept;
};
}  // namespace ls3

﻿#include "ls3Vorschaupp.h"

#include <stdexcept>

static_assert(sizeof(char*) == 4, "This library supports 32bit builds only");

namespace ls3 {
void ls3Vorschaupp::doLoad() {
    hinstLib = LoadLibrary(TEXT("z3strbie.dll"));

    if (!hinstLib) {
        throw std::runtime_error("Failed to laod dll");
    }

    _ls3Vorschau = (ls3Vorschau_t)GetProcAddress(hinstLib, "ls3Vorschau");
    if (!_ls3Vorschau) {
        FreeLibrary(hinstLib);
        throw std::runtime_error("Failed to resolve symbol");
    }
}

ls3Vorschaupp::ls3Vorschaupp(HWND hwnd, LPCSTR dllDir, LPCSTR baseDir)
    : baseDir{baseDir}, hwnd{hwnd} {
    SetDllDirectoryA(dllDir);
    doLoad();
}

ls3Vorschaupp::ls3Vorschaupp(HWND hwnd, LPCWSTR dllDir, LPCSTR baseDir)
    : baseDir{baseDir}, hwnd{hwnd} {
    SetDllDirectoryW(dllDir);
    doLoad();
}

ls3Vorschaupp::~ls3Vorschaupp() {
    // Unloading the library unfortunately blocks. We have to leak ...
    // FreeLibrary(hinstLib);
}

bool ls3Vorschaupp::generate(const char* DateiName, const char* BMPDateiname, AnsichtModus Modus,
                               Size size, FileFormat format) const noexcept {
    auto result =
        _ls3Vorschau(baseDir.c_str(), DateiName, BMPDateiname, Modus, hwnd, size.x, size.y, format);
    return result;
}
}  // namespace ls3
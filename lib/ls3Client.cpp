#include "ls3Client.h"

#include <strsafe.h>
#include <array>
#include <iostream>
#include <vector>

#include "json-builder.h"

namespace ls3 {
namespace {
using ujson_value = std::unique_ptr<json_value, decltype(&json_builder_free)>;
template <typename Func, typename... Arguments>
ujson_value make_unique_json(Func&& f, Arguments&&... args) {
    return {f(std::forward<Arguments>(args)...), &json_builder_free};
}

template <size_t N>
auto json_object_push_literal(ujson_value& obj, const char (&key)[N], json_value* value) {
    return json_object_push_length(obj.get(), N - 1, const_cast<char*>(key), value);
}
}  // namespace

ls3Client::ls3Client(LPCTSTR pathToExe, LPCTSTR dllDir, LPCTSTR baseDir) {
    hJob = CreateJobObject(nullptr, TEXT("ls3Job"));
    if (!hJob) {
        MessageBox(0, TEXT("Could not create JobObject"),
                   TEXT("The program might crash any moment now, sorry :("), MB_OK);
        // std::terminate();
        return;
    }

    JOBOBJECT_EXTENDED_LIMIT_INFORMATION jeli{0};
    jeli.BasicLimitInformation.LimitFlags = JOB_OBJECT_LIMIT_KILL_ON_JOB_CLOSE;
    if (!SetInformationJobObject(hJob, JobObjectExtendedLimitInformation, &jeli, sizeof(jeli))) {
        MessageBox(0, TEXT("Could not set JobOject JELI"),
                   TEXT("The program might crash any moment now, sorry :("), MB_OK);
        // std::terminate();
        return;
    }

    SECURITY_ATTRIBUTES saAttr{};
    saAttr.nLength = sizeof(SECURITY_ATTRIBUTES);
    saAttr.bInheritHandle = TRUE;
    saAttr.lpSecurityDescriptor = nullptr;

    if (!CreatePipe(&hChildStdOutRd, &hChildStdOutWr, &saAttr, 0) ||
        !SetHandleInformation(hChildStdOutRd, HANDLE_FLAG_INHERIT, 0) ||
        !CreatePipe(&hChildStdInRd, &hChildStdInWr, &saAttr, 0) ||
        !SetHandleInformation(hChildStdInWr, HANDLE_FLAG_INHERIT, 0)) {
        std::cout << "A" << std::endl;
    }

    STARTUPINFO siStartInfo{};

    siStartInfo.cb = sizeof(STARTUPINFO);
    // siStartInfo.hStdError = hChildStdInWr;
    siStartInfo.hStdOutput = hChildStdOutWr;
    siStartInfo.hStdInput = hChildStdInRd;
    siStartInfo.dwFlags |= STARTF_USESTDHANDLES;

    auto cmd = std::array<TCHAR, 1000>{};
    cmd[0] = '"';
    cmd[1] = '\0';
    // This is not hte best way - ignores " in paths, is inefficient, etc.
    if (StringCchCat(&cmd[0], cmd.size(), pathToExe) != S_OK ||
        StringCchCat(&cmd[0], cmd.size(), TEXT("\" \"")) != S_OK ||
        StringCchCat(&cmd[0], cmd.size(), dllDir) != S_OK ||
        StringCchCat(&cmd[0], cmd.size(), TEXT("\" \"")) != S_OK ||
        StringCchCat(&cmd[0], cmd.size(), baseDir) != S_OK ||
        StringCchCat(&cmd[0], cmd.size(), TEXT("\"")) != S_OK) {
        //std::cout << "Command line: " << cmd.data() << std::endl;
        throw 5;
    }

    if (!CreateProcess(nullptr, cmd.data(), nullptr, nullptr, true, 0, nullptr, nullptr,
                       &siStartInfo, &piProcInfo)) {
        std::cerr << "ERROR " << GetLastError() << std::endl;
        // throw 1;
        return;
    }

    AssignProcessToJobObject(hJob, piProcInfo.hProcess);
}

ls3Client::~ls3Client() {
    CloseHandle(piProcInfo.hProcess);
    CloseHandle(piProcInfo.hThread);

    CloseHandle(hChildStdInRd);
    CloseHandle(hChildStdInWr);
    CloseHandle(hChildStdOutRd);
    CloseHandle(hChildStdOutWr);

    CloseHandle(hJob);
}

bool ls3Client::generate(const char* DateiName, const char* BMPDateiname, AnsichtModus Modus,
                         Size size, FileFormat format) const noexcept {
    auto data = make_unique_json(&json_object_new, 5);

    json_object_push_literal(data, "filename", json_string_new(DateiName));
    json_object_push_literal(data, "target", json_string_new(BMPDateiname));
    json_object_push_literal(data, "mode", json_integer_new(static_cast<json_int_t>(Modus)));

    auto size_j = json_array_new(2);
    json_array_push(size_j, json_integer_new(size.x));
    json_array_push(size_j, json_integer_new(size.y));
    json_object_push_literal(data, "size", size_j);

    json_object_push_literal(data, "format", json_integer_new(static_cast<json_int_t>(format)));

    auto length = json_measure(data.get());
    std::vector<char> vec{};
    vec.resize(length + 1);
    json_serialize(&vec[0], data.get());
    vec[length - 1] = '\n';
    vec[length] = '\0';

    std::string length_s{std::to_string(length)};
    //  TODO: expected length is DWORD!
    WriteFile(hChildStdInWr, length_s.c_str(), length_s.length(), nullptr, nullptr);
    WriteFile(hChildStdInWr, vec.data(), length, nullptr, nullptr);

    return true;
}
}  // namespace ls3
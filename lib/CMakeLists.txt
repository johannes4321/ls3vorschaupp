﻿cmake_minimum_required (VERSION 3.8)

add_library(json-parser STATIC "json.c" "json.h")

if(CMAKE_SIZEOF_VOID_P EQUAL 4)
  add_library (ls3Vorschaupp STATIC "ls3Vorschaupp.cpp" "ls3Vorschaupp.h" "ls3common.h")
endif()
  
add_library (ls3clientlib STATIC "ls3Client.cpp" "ls3Client.h" "ls3common.h" "json-builder.c" "json-builder.h")
target_link_libraries(ls3clientlib json-parser)

add_library (ls3clientlibw STATIC "ls3Client.cpp" "ls3Client.h" "ls3common.h" "json-builder.c" "json-builder.h")
target_link_libraries(ls3clientlib json-parser)
target_compile_definitions(ls3clientlibw PUBLIC UNICODE)
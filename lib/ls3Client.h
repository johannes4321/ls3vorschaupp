#pragma once

#include "ls3common.h"

#include <Windows.h>
#include <memory>

namespace ls3 {
namespace {
using unique_handle = std::unique_ptr<std::remove_pointer_t<HANDLE>, decltype(&CloseHandle)>;
unique_handle make_unique_handle(HANDLE h) {
    return unique_handle{h, &CloseHandle};
}
}  // namespace

class ls3Client : public ls3interface<ls3Client> {
    friend ls3interface<ls3Client>;

    HANDLE hJob{};
    HANDLE hChildStdInRd{};
    HANDLE hChildStdInWr{};
    HANDLE hChildStdOutRd{};
    HANDLE hChildStdOutWr{};

    PROCESS_INFORMATION piProcInfo{};

   public:
    ls3Client(LPCTSTR pathToExe, LPCTSTR dllDir, LPCTSTR baseDir);

    ls3Client(const ls3Client&) = delete;
    ls3Client& operator=(const ls3Client&) = delete;

    ls3Client(ls3Client&&) = default;

    ~ls3Client();

   private:
    bool generate(const char* DateiName, const char* BMPDateiname, AnsichtModus Modus, Size size,
                  FileFormat format) const noexcept;
};
}  // namespace ls3
#ifndef REMOTE
#include "ls3Vorschaupp.h"
#else
#include "ls3Client.h"
#endif

#include <iostream>

int main() {
    std::string s;

#ifndef REMOTE
    ls3::ls3Vorschaupp ls3Vorschau{(LPTSTR)TEXT("c:\\Program Files (x86)\\Zusi3"),
                               "c:\\Program Files (x86)\\Zusi3\\_ZusiData"};
#else
    ls3::ls3Client ls3Vorschau{(LPTSTR)TEXT("ls3vorschau.exe"),
                               (LPTSTR)TEXT("c:\\Program Files (x86)\\Zusi3"),
                               (LPTSTR)TEXT("c:\\Program Files (x86)\\Zusi3\\_ZusiData")};
#endif

    ls3Vorschau(
        "RollingStock\\Deutschland\\Epoche6\\Elektrotriebwagen\\I"
        "CE4\\3D-Daten\\TK.lod3.ls3",
        "testj2", ls3::AnsichtModus::Fahrzeugansicht, {300, 300}, ls3::FileFormat::PNG);

    Sleep(5000);

    return 0;
}
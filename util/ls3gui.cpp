/**
* ls3GUI
* 
* Simple GUI tool to experiment with the ls3 library and win32 API
* 
* Design notes:
* This is my first "serious" win32 API program ever. Some API usage might not
* be perfect. To satisfy my curiosity this makes *no* use of MFC, ATL or
* similar libraries.
* It is also a goal to work with and without UNICODE defined and either use
* the "remote" API or the direct API. All that with a single #ifdef and then
* only templates.
* There are some known leaks (like around char-string and wstring conversion)
* which eventually should be fixed by improving my conversion routines and
* their usage.
* As this is my learning utility don't take anything as an example for
* your program :-)
*/

#include <ShObjIdl.h>
#include <Windows.h>
#include <windowsx.h>

#include <concepts>
#include <memory>
#include <tuple>

#ifdef REMOTE
#include "ls3Client.h"
using Ls3Vorschau = ls3::ls3Client;
using ls3Args = std::tuple<LPCTSTR, LPCTSTR, LPCTSTR>;
#else
#include "ls3Vorschaupp.h"
using Ls3Vorschau = ls3::ls3Vorschaupp;
using ls3Args = std::tuple<HWND, LPCTSTR, LPCSTR>;
#endif

namespace {
class IUGuard {
    IUnknown *c;

   public:
    IUGuard(IUnknown *c) : c{c} {
    }
    ~IUGuard() {
        c && c->Release();
    }
};

template <typename StrType>
struct to {};

template <>
struct to<LPCWSTR> {
    static LPCWSTR str(LPCWSTR str) {
        // TODO - this should probbly copy so caller can always delete
        return str;
    }
    static LPCWSTR str(LPCSTR str) {
        size_t resultlen;
        auto len = strlen(str);
        // yikes, a "new"
        auto result = new WCHAR[len + 1];
        mbstowcs_s(&resultlen, result, len, str, len);
        return result;
    }
};

template <>
struct to<const CHAR *> {
    static LPCSTR str(LPCSTR str) {
        // TODO - this should probbly copy so caller can always delete
        return str;
    }
    static LPCSTR str(LPCWSTR str) {
        size_t resultlen;
        auto len = wcslen(str);
        // yikes, a "new" and a C array and a guessed length!
        auto result = new CHAR[len * 2 + 1];

        wcstombs_s(&resultlen, result, len * 2 + 1, str, len);
        return result;
    }
};

template <>
struct to<CHAR *> : to<const CHAR *> {};

template <>
struct to<HWND> {
    static HWND str(HWND hwnd) {
        return hwnd;
    }
};

template <typename T>
concept has_wndproc = requires(T *t, HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) {
    { t->WndProc(hwnd, message, wParam, lParam) }
    ->std::convertible_to<LRESULT>;
};

template <has_wndproc WindowType>
LRESULT CALLBACK GWndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) {
    WindowType *pThis{};
    if (message == WM_CREATE) {
        LPCREATESTRUCT lpcs = reinterpret_cast<LPCREATESTRUCT>(lParam);
        pThis = static_cast<WindowType *>(lpcs->lpCreateParams);
        SetWindowLongPtr(hwnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(pThis));
    } else {
        pThis = reinterpret_cast<WindowType *>(GetWindowLongPtr(hwnd, GWLP_USERDATA));
    }

    if (!pThis) {
        return DefWindowProc(hwnd, message, wParam, lParam);
    }

    return pThis->WndProc(hwnd, message, wParam, lParam);
}

// std::unique_ptr<std::remove_pointer<PWSTR>::type, decltype(&CoTaskMemFree)>

/*
This seems impossible, since FilePicker isn't declared here, yet ...
template <typename T>
concept FilePickerImpl = std::derived_from<T, FilePicker<T>>;
*/

// MSVC doesn't think thoise are satisfied by the implementations, but errors lead me nowhere good
template <typename T>
concept FilePickerImpl = has_wndproc<T> &&std::convertible_to<decltype(T::szWindowClass), LPTSTR>;

template <typename Derived>
class FilePicker {
   protected:
    IFileOpenDialog *pFileOpen{};

   private:
    HINSTANCE hInstance{};
    HMENU menu{};
    HWND hWnd{};
    HWND hLabel{};

    std::unique_ptr<std::remove_pointer<PWSTR>::type, decltype(&CoTaskMemFree)> uspResult{
        nullptr, &CoTaskMemFree};

   public:
    FilePicker(HINSTANCE hInstance, HWND hwnd, LPCTSTR label, HMENU menu, int x, int y, int width,
               int height)
        : hInstance{hInstance}, menu{menu} {
        // TODO: handle error ;)
        (void)CoCreateInstance(CLSID_FileOpenDialog, nullptr, CLSCTX_ALL, IID_IFileOpenDialog,
                               reinterpret_cast<void **>(&pFileOpen));

        this->hWnd = CreateWindow(Derived::szWindowClass, label, WS_VISIBLE | WS_CHILD, x, y, width,
                                  height, hwnd, nullptr, hInstance, this);
    }

    ~FilePicker() {
        pFileOpen->Release();
    }

    bool valid() const {
        return !!uspResult;
    }

    const PWSTR getResult() const {
        return uspResult.get();
    }

    LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) {
        switch (message) {
            case WM_CREATE: {
                RECT rect{};
                GetWindowRect(hwnd, &rect);
                const int x = 0, y = 0;
                const int width = rect.right - rect.left;
                const int height = rect.bottom - rect.top;

                TCHAR text[200];
                GetWindowText(hwnd, text, 200);
                CreateWindow(TEXT("BUTTON"), text,
                             WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, x, y, 130,
                             height, hwnd, (HMENU)101, hInstance, 0);

                hLabel = CreateWindow(TEXT("EDIT"),
                                      TEXT("Click den Button links und erlebe dein blaues wunder"),
                                      WS_VISIBLE | WS_CHILD, 150, y + 5, width - x - 150, height,
                                      hwnd, 0, hInstance, 0);

                return 0;
            }
            case WM_COMMAND:
                switch (LOWORD(wParam)) {
                    case 101:
                        return pick(hwnd);
                }
                break;
        }
        return DefWindowProc(hwnd, message, wParam, lParam);
    }

    static ATOM registerClass(HINSTANCE hInstance) {
        WNDCLASSEX wcex;
        wcex.cbSize = sizeof(WNDCLASSEX);
        wcex.style = CS_HREDRAW | CS_VREDRAW;
        wcex.lpfnWndProc = GWndProc<Derived>;
        wcex.cbClsExtra = 0;
        wcex.cbWndExtra = 0;
        wcex.hInstance = hInstance;
        wcex.hIcon = LoadIcon(hInstance, IDI_APPLICATION);
        wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
        wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
        wcex.lpszMenuName = nullptr;
        wcex.lpszClassName = Derived::szWindowClass;
        wcex.hIconSm = LoadIcon(wcex.hInstance, IDI_APPLICATION);

        return RegisterClassEx(&wcex);
    }

    LRESULT pick(HWND hwnd) {
        // TODO revisit return values
        if (!SUCCEEDED(pFileOpen->Show(hwnd))) {
            return 0;
        }

        IShellItem *pItem;
        if (!SUCCEEDED(pFileOpen->GetResult(&pItem))) {
            return 0;
        }
        IUGuard guard{pItem};

        PWSTR ptzFilePath;
        if (!SUCCEEDED(pItem->GetDisplayName(SIGDN_FILESYSPATH, &ptzFilePath))) {
            return 0;
        }

        hLabel &&SetWindowTextW(hLabel, ptzFilePath);
        uspResult.reset(ptzFilePath);

        PostMessage(GetParent(hwnd), WM_COMMAND, (WPARAM)menu, 0);

        return 0;
    }
};

class BackendFile : public FilePicker<BackendFile> {
    friend FilePicker<BackendFile>;
    static inline const TCHAR szWindowClass[] = TEXT("BackendFileWindow");

    static const inline _COMDLG_FILTERSPEC rgSpec[] = {
        {L"Executable Files", L"*.exe"},
    };

   public:
    BackendFile(HINSTANCE hInstance, HWND hwnd, LPCTSTR label, HMENU menu, int x, int y, int width,
                int height)
        : FilePicker{hInstance, hwnd, label, menu, x, y, width, height} {
        pFileOpen->SetFileTypes(1, rgSpec);
    }
};

template <typename ResultType>
struct Picker {};

template <>
class Picker<HWND> final {
    HWND hwnd{};

   public:
    static const inline int rows = 0;

    Picker(HINSTANCE, HWND hwnd, LPCTSTR label, HMENU menu, int x, int y, int width, int height) {
        this->hwnd = hwnd;
    }

    void pick(HWND) const {
    }

    bool valid() const {
        return true;
    }

    HWND getResult() const {
        return hwnd;
    }
};

template <>
struct Picker<LPCTSTR> final : public BackendFile {
    static const inline int rows = 1;

    Picker(HINSTANCE hInstance, HWND hwnd, LPCTSTR label, HMENU menu, int x, int y, int width,
           int height)
        : BackendFile{hInstance, hwnd, label, menu, x, y, width, height} {
    }
};

class FolderPicker final : public FilePicker<FolderPicker> {
    friend FilePicker<FolderPicker>;

   public:
    static inline const TCHAR szWindowClass[] = TEXT("FolderPickerWindow");
    FolderPicker(HINSTANCE hInstance, HWND hwnd, LPTSTR label, HMENU menu, int x, int y, int width,
                 int height)
        : FilePicker{hInstance, hwnd, label, menu, x, y, width, height} {
        pFileOpen->SetOptions(FOS_PICKFOLDERS);
    }
};

using FirstArgPicker = Picker<std::tuple_element_t<0, ls3Args>>;

inline int ROW(int r) {
    return r * 40 + 10;
}

class ls3StartupWindow final {
    static inline const TCHAR szWindowClass[] = TEXT("ls3Startup");

    static inline const TCHAR szTitle[] = TEXT("ls3Vorschau Utility");

    std::unique_ptr<FirstArgPicker> pBackendFile{nullptr};
    std::unique_ptr<FolderPicker> pZusiDir{nullptr};
    std::unique_ptr<FolderPicker> pZusiDataDir{nullptr};

    HWND hContinueBtn{};
    HINSTANCE hInstance{};
    HMENU hmenu{};
    HWND hWnd{};

    ls3Args args{};

    void setupWindow(HWND hwnd) {
        int row{0};
        const int width = 500;

        pBackendFile.reset(new FirstArgPicker(hInstance, hwnd, (LPTSTR)TEXT("Pick Server"),
                                              (HMENU)101, 10, 10, width, 30));
        row += FirstArgPicker::rows;

        pZusiDir.reset(new FolderPicker(hInstance, hwnd, (LPTSTR)TEXT("Program Dir"), (HMENU)102,
                                        10, ROW(row++), width, 30));

        pZusiDataDir.reset(new FolderPicker(hInstance, hwnd, (LPTSTR)TEXT("Program Data Dir"),
                                            (HMENU)103, 10, ROW(row++), width, 30));

        hContinueBtn =
            CreateWindow(TEXT("BUTTON"), TEXT("Continue"),
                         WS_TABSTOP | WS_VISIBLE | WS_CHILD | WS_DISABLED | BS_DEFPUSHBUTTON,
                         width - 10 - 130 - 10, ROW(row), 130, 30, hwnd, (HMENU)104, hInstance, 0);

        /*CreateWindow(TEXT("COMBOBOX"), TEXT(""),
                     CBS_DROPDOWNLIST | CBS_HASSTRINGS | WS_CHILD | WS_OVERLAPPED | WS_VISIBLE, 10,
                     ROW(row), 200, 200, hwnd, nullptr, hInstance, nullptr);*/
    }

   public:
    ls3StartupWindow(HINSTANCE hInstance, HWND hwndParent, HMENU hmenu, int x, int y, int width,
                     int height)
        : hInstance{hInstance},
          hmenu{hmenu},
          hWnd{CreateWindow(szWindowClass, szTitle, WS_CHILD | WS_VISIBLE | WS_CHILD, x, y, width,
                            height, hwndParent, nullptr, hInstance, this)} {
    }

    static ATOM registerClass(HINSTANCE hInstance) {
        WNDCLASSEX wcex;
        wcex.cbSize = sizeof(WNDCLASSEX);
        wcex.style = CS_HREDRAW | CS_VREDRAW;
        wcex.lpfnWndProc = GWndProc<ls3StartupWindow>;
        wcex.cbClsExtra = 0;
        wcex.cbWndExtra = 0;
        wcex.hInstance = hInstance;
        wcex.hIcon = LoadIcon(hInstance, IDI_APPLICATION);
        wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
        wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
        wcex.lpszMenuName = nullptr;
        wcex.lpszClassName = szWindowClass;
        wcex.hIconSm = LoadIcon(wcex.hInstance, IDI_APPLICATION);

        return RegisterClassEx(&wcex);
    }

    void show(int nCmdShow) {
        ShowWindow(hWnd, nCmdShow);
        UpdateWindow(hWnd);
    }

    LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) {
        switch (message) {
            case WM_CREATE:
                setupWindow(hwnd);
                break;
            case WM_COMMAND:
                Button_Enable(hContinueBtn,
                              pBackendFile->valid() && pZusiDir->valid() && pZusiDataDir->valid());

                using FirstArgPicker = Picker<std::tuple_element_t<0, ls3Args>>;

                if (pBackendFile->valid() && pZusiDir->valid() && pZusiDataDir->valid()) {
                    std::get<0>(args) =
                        to<std::tuple_element_t<0, ls3Args>>::str(pBackendFile->getResult());
                    std::get<1>(args) =
                        to<std::tuple_element_t<1, ls3Args>>::str(pZusiDir->getResult());
                    std::get<2>(args) =
                        to<std::tuple_element_t<2, ls3Args>>::str(pZusiDataDir->getResult());

                    PostMessage(GetParent(hwnd), WM_COMMAND, (WPARAM)hmenu, (LPARAM)&args);

                    Ls3Vorschau ls3{std::get<0>(args), std::get<1>(args), std::get<2>(args)};
                    ls3("RollingStock\\Deutschland\\Epoche6\\Elektrotriebwagen\\I"
                        "CE4\\3D-Daten\\TK.lod3.ls3",
                        "testj2", ls3::AnsichtModus::Fahrzeugansicht, {300, 300},
                        ls3::FileFormat::PNG);
                }
                break;
            case WM_DESTROY:
                PostQuitMessage(0);
                break;
            default:
                return DefWindowProc(hwnd, message, wParam, lParam);
        }

        return 0;
    }
};

class mainwindow final {
    static inline const TCHAR szWindowClass[] = TEXT("mainwindow");

    static inline const TCHAR szTitle[] = TEXT("ls3Vorschau Utility");

    HINSTANCE hInstance{};
    HWND hwnd{};

    void setupWindow(HWND hwnd) {
        ls3StartupWindow window{hInstance, hwnd, (HMENU)101, 0, 0, 500, 300};
    }

   public:
    mainwindow(HINSTANCE hInstance)
        : hInstance{hInstance},
          hwnd{CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW, CW_USEDEFAULT,
                            CW_USEDEFAULT, 500, 300, nullptr, nullptr, hInstance, this)} {
    }

    static ATOM registerClass(HINSTANCE hInstance) {
        WNDCLASSEX wcex;
        wcex.cbSize = sizeof(WNDCLASSEX);
        wcex.style = CS_HREDRAW | CS_VREDRAW;
        wcex.lpfnWndProc = GWndProc<mainwindow>;
        wcex.cbClsExtra = 0;
        wcex.cbWndExtra = 0;
        wcex.hInstance = hInstance;
        wcex.hIcon = LoadIcon(hInstance, IDI_APPLICATION);
        wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
        wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
        wcex.lpszMenuName = nullptr;
        wcex.lpszClassName = szWindowClass;
        wcex.hIconSm = LoadIcon(wcex.hInstance, IDI_APPLICATION);

        return RegisterClassEx(&wcex);
    }

    LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) {
        switch (message) {
            case WM_CREATE:
                setupWindow(hwnd);
                break;
            case WM_COMMAND:
                /*
                    Ls3Vorschau ls3{std::get<0>(args), std::get<1>(args), std::get<2>(args)};
                    ls3("RollingStock\\Deutschland\\Epoche6\\Elektrotriebwagen\\I"
                        "CE4\\3D-Daten\\TK.lod3.ls3",
                        "testj2", ls3::AnsichtModus::Fahrzeugansicht, {300, 300},
                        ls3::FileFormat::PNG);
                */
                break;
            case WM_DESTROY:
                PostQuitMessage(0);
                break;
            default:
                return DefWindowProc(hwnd, message, wParam, lParam);
        }

        return 0;
    }

    void show(int nCmdShow) {
        ShowWindow(hwnd, nCmdShow);
        UpdateWindow(hwnd);
    }
};
}  // namespace

int __stdcall WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
    if (!SUCCEEDED(CoInitializeEx(nullptr, COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE))) {
        return 1;
    }

    BackendFile::registerClass(hInstance);
    FolderPicker::registerClass(hInstance);
    ls3StartupWindow::registerClass(hInstance);
    mainwindow::registerClass(hInstance);

    MSG msg;
    {
        mainwindow window{hInstance};
        window.show(nCmdShow);

        while (GetMessage(&msg, nullptr, 0, 0)) {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    CoUninitialize();

    return (int)msg.wParam;
}
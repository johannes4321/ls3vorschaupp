#include "ls3Vorschaupp.h"

#include <iostream>
#include <vector>

#include "json.h"

namespace {
using ujson_value = std::unique_ptr<json_value, decltype(&::json_value_free)>;

template <typename T, typename VT = T>
bool between(VT value, T lower, T upper) {
    return value >= static_cast<VT>(lower) && value <= static_cast<VT>(upper);
}

ls3::request request_from_json(const ujson_value &json) {
    ls3::request request{};

     for (auto &&objelement : json->u.object) {
        using namespace std::string_literals;

        if ("filename"s == objelement.name && objelement.value->type == json_string) {
            request.filename = objelement.value->u.string.ptr;
        } else if ("target"s == objelement.name && objelement.value->type == json_string) {
            request.target = objelement.value->u.string.ptr;
        } else if ("mode"s == objelement.name && objelement.value->type == json_integer) {
            json_int_t mode = objelement.value->u.integer;
            if (between(mode, ls3::AnsichtModus::Fahrzeugansicht, ls3::AnsichtModus::Seite2)) {
                request.mode = static_cast<ls3::AnsichtModus>(mode);
            }
        } else if ("size"s == objelement.name && objelement.value->type == json_array &&
                   objelement.value->u.array.length == 2 &&
                   objelement.value->u.array.values[0]->type == json_integer &&
                   objelement.value->u.array.values[1]->type == json_integer) {
            request.size = {static_cast<int32_t>(objelement.value->u.array.values[0]->u.integer),
                            static_cast<int32_t>(objelement.value->u.array.values[1]->u.integer)};
        } else if ("format"s == objelement.name && objelement.value->type == json_integer) {
            json_int_t format = objelement.value->u.integer;
            if (between(format, ls3::FileFormat::BMP, ls3::FileFormat::HDR)) {
                request.format = static_cast<ls3::FileFormat>(format);
            }
        }
    }

    return request;
}

void loop(const ls3::ls3Vorschaupp &ls3vorschau) noexcept {
    while (true) {
        int len;
        std::cin >> len;

        if (len == -1) {
            return;
        }

        std::vector<char> json_raw(len + 1);
        std::cin.read(json_raw.data(), len);

        std::cout << "READ: " << json_raw.data() << std::endl;

        auto json = ujson_value{json_parse(json_raw.data(), len), &json_value_free};

        if (!json || json->type != json_object) {
            std::cerr << "Failed to parse " << json_raw.data() << std::endl;
            continue;
        }

        auto request = request_from_json(json);
        ls3vorschau(request);
    }
}
}  // namespace

int main(int argc, char *argv[]) {
    if (argc != 3) {
        std::cerr << "Wrong invocation!\nUsage: " << argv[0] << " zusiProgramDir zusiDataDir"
                  << std::endl;
        return 1;
    }

    try {
        ls3::ls3Vorschaupp ls3vorschau{0, argv[1], argv[2]};
        loop(ls3vorschau);
    } catch (std::runtime_error &e) {
        std::cerr << "FAILURE: " << e.what() << std::endl;
        return 1;
    }
}

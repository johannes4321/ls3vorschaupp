Zusi 3 ls3Vorschau wrapper library for C++
========================================

This library is a simple wrapper to call ls3Vorschau from Zusi3's
`z3strbie.dll`. Thus it allows to create rendered images of ls3 
3D model files. If you don't understand what that means you
probably don't need this library. :-)

For usage from 64bit binaries it also provides a two-process mode, where
a 64bit process controls a 32bit process generating the rendered images.

Example usage
-------------
See the bundled util.

    int main() {
        try {
            ls3::ls3Vorschaupp ls3vorschau{
                0, // HWND if you need/want
                TEXT("C:\\Program Files (x86)\\Zusi3"),
                "C:\\Program Files (x86)\\Zusi3\\_ZusiData"
            };
  
            ls3vorschau(
                "RollingStock\\Deutschland\\Epoche3\\Dieselloks\\BRD\\V60\\3D-Daten\\db_260_ar.lod.ls3",
                "test",
                ls3::AnsichtModus::Fahrzeugansicht,
                { 300, 300 },
                ls3::FileFormat::PNG
            );
        } catch (std::runtime_error& e) {
            // ....
            return 1;
        }
    
        return 0;
    }

This will create a file `test.png` (Zusi will add the file extension itself) with a rendered view
of that V60 engine with 300x300 pixel.

Two-Process mode
----------------
For using the two process mode you need to compile this once in 32bit mode and
note the location of the generated `ls3Vorschau.exe`. The client library then
can be used from a 32 or 64 bit build and has a similar API to the single
process version:

    int main() {
        try {
            ls3::ls3Client ls3vorschau{
                TEXT("..\\..\\32bit\\util\\ls3Vorschau.exe"),
                TEXT("C:\\Program Files (x86)\\Zusi3"),
                TEXT("C:\\Program Files (x86)\\Zusi3\\_ZusiData")
            };
  
            ls3vorschau(
                "RollingStock\\Deutschland\\Epoche3\\Dieselloks\\BRD\\V60\\3D-Daten\\db_260_ar.lod.ls3",
                "test",
                ls3::AnsichtModus::Fahrzeugansicht,
                { 300, 300 },
                ls3::FileFormat::PNG
            );
        } catch (std::runtime_error& e) {
            // ....
            return 1;
        }
    
        return 0;
    }

You might notice that this is almost exactly the same, jsut that we are using
`ls3::ls3Client` instead of `ls3::ls3Vorschaupp` and in the constructor
parameters the path to exe file instead of the `HWND`, also the base path to
the Zusi data dir is using a `lptstr` over a `char*` as it is passed through
Windows APIs and not directly into Zusi.

Note: This should work with `#define UNICODE` but hasn't been tested.

Filename helper
---------------

    namespace ls3 {
        std::string fullFilename(const char* base, FileFormat format);
    }

Since Zusi will add the file extension automatically to the generated file this
helper function provides the filename by appending the file extension.

Request object
--------------
To ease usage a request object can be used, so arguments can be handled easily:

    ls3::ls3Client ls3vorschau{/* .... */};

    ls3::request request{};
    request.filename = "RollingStock\\Deutschland\\Epoche3\\Dieselloks\\BRD\\V60\\3D-Daten\\db_260_ar.lod.ls3";
    request.target = "v60_preview";
    request.mode = ls3::AnsichtModus::Fahrzeugansicht;
    request.size = { 100, 100 };
    request.format = ls3::FileFormat::PNG;

    if (ls3vorschau(request)) {
        std::cout << "Created file: " << ls3::fullFilename(request) << std::endl;
    }

This works with both, `ls3Vorschaupp` and `ls3Client`.

Stability guarantees
--------------------
None. all APIs can change all the time. If you are planning to use the library
please let me know, so I can coordinate breakage.

Also mind that the version of the `z3strbie.dll` is not checked in any way. If
the library's binary interface changes this can lead to all sorts of issues. The
two-process mode could isolate against some issues.

License
-------
Licensed under terms of the GPLv3, Copyright (C) 2021 Johannes Schl�ter.

This software includes json-parser Copyright (C) 2012, 2013, 2014 James McLaughlin et al. 
All rights reserved. https://github.com/udp/json-parser

 This software includes json-builder Copyright (C) 2014 James McLaughlin.  All
 rights reserved. https://github.com/udp/json-builder

Zusi is a trademark by Carsten H�lscher.